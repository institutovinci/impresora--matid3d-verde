# impresora--matid3d-verde

Información y configuración de la impresora Matid 3D de color verde (símil Folger Tech RepRap 2020 Prusa i3).

## Estado

	* Marlin 1.1.8
	* Funcionando todo menos la carga automática de la SD al reinsertarla.
	* El slot de Extrusor0 está quemado. Por eso cambié el driver al Extrusor1.
	
## Perfiles

### Ultimaker Cura

_Esto está desactualizado. Lo cambiamos por https://gitlab.com/institutovinci/perfiles-cura y https://gitlab.com/institutovinci/scripts-actualizacion--automatica-cura ._

[Cómo cargar impresoras y perfiles en Ultimaker Cura](https://www.youtube.com/watch?v=sRDgST4yU9A)

- `matid3d-verde.curaproject`: definición de la máquina.
- `matid3dverde-v2-02mm-50mms.curaprofile`: perfil de base. 0,2 mm de altura de capa y 50 mm/s de velocidad base.